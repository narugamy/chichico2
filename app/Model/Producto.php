<?php

	namespace App\Model;

	use Illuminate\Database\Eloquent\Model;
	use Illuminate\Database\Eloquent\SoftDeletes;

	class Producto extends Model {

		protected $table = "producto";

		use SoftDeletes;

		protected $fillable = ['nombre', 'descripcion', 'stock', 'slug'];

		public function conceptos() {
			return $this->belongsToMany(Rol::class);
		}

	}
