<?php

	namespace App\Model;

	use Illuminate\Database\Eloquent\Model;
	use Illuminate\Database\Eloquent\SoftDeletes;

	class Rol extends Model {

		protected $table = "rol";

		use SoftDeletes;

		protected $fillable = ['name', 'slug'];

		public function usuarios() {
			return $this->hasMany(Usuario::class);
		}

	}
