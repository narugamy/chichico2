<?php

	namespace App\Model;

	use Illuminate\Database\Eloquent\Model;
	use Illuminate\Database\Eloquent\SoftDeletes;

	class Tipo extends Model {

		protected $table = "tipo";

		use SoftDeletes;

		protected $fillable = ['name', 'slug'];

		public function conceptos() {
			return $this->hasMany(Concepto::class);
		}

	}
