<?php

	namespace App\Model;

	use Illuminate\Database\Eloquent\Model;
	use Illuminate\Database\Eloquent\SoftDeletes;

	class Concepto extends Model {

		protected $table = "concepto";

		use SoftDeletes;

		protected $fillable = ['usuario_id', 'tipo_id', 'codigo', 'empresa', 'fecha', 'estado'];

		public function productos() {
			return $this->belongsToMany(Producto::class)->withPivot('cantidad');
		}

		public function tipo() {
			return $this->belongsTo(Tipo::class);
		}

		public function usuario() {
			return $this->belongsTo(Usuario::class);
		}

	}
