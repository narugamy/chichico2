<?php

	namespace App\Model;

	use Illuminate\Database\Eloquent\SoftDeletes;
	use Illuminate\Foundation\Auth\User as Authenticatable;

	class Usuario extends Authenticatable {

		protected $table = "usuario";

		use SoftDeletes;

		protected $fillable = ['rol_id', 'nombres', 'apellidos', 'usuario', 'password'];

		protected $hidden = ['password', 'remember_token',];

		public function rol() {
			return $this->belongsTo(Rol::class);
		}

		public function full_name(){
			return "$this->nombres $this->apellidos";
		}

	}
