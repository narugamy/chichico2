<?php

	namespace App\Http\Requests\Admin\Login;

	use Illuminate\Foundation\Http\FormRequest;

	class LoginRequest extends FormRequest {

		public function authorize() {
			return true;
		}

		public function rules() {
			return [
				'usuario' => 'required|min:5|max:21|exists:usuario',
				'password' => 'required|min:3|max:255'
			];
		}

		public function messages(){
			return [
				'usuario.required'=>'requerido',
				'usuario.exists'=>'el usuario no existe',
				'usuario.min'=>'min. :min caracteres',
				'usuario.max'=>'max. :max caracteres',
				'password.required'=>'requerido',
				'password.min'=>'min. :min caracteres',
				'password.max'=>'max. :max caracteres'
			];
		}

	}
