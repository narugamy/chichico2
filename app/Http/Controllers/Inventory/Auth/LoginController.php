<?php

	namespace App\Http\Controllers\Inventory\Auth;

	use App\Http\Requests\Admin\Login\LoginRequest;
	use App\Model\Usuario;
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use Illuminate\Support\Facades\Auth;
	use Illuminate\Support\Facades\Hash;

	class LoginController extends Controller {

		public function __construct() {
			$this->middleware('guest:client')->except('destroy');
		}

		public function showLoginForm() {
			return view('client.auth.app');
		}

		public function login(LoginRequest $request) {
			$user = Usuario::where(['usuario' => $request->usuario])->with('rol')->first();
			if ($user):
				if (Hash::check($request->password, $user->password)):
					if ($user->rol->slug === 'usuario'):
						Auth::guard('client')->login($user);
						$message = 'Registro exitoso';
						$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('client.index'), 'errors' => null], 'status' => 200, 'route' => route('client.index'), 'message' => $message, 'type' => 'success'];
						$data = $this->optimize($array);
					else:
						$message = 'No tienes el rol correcto';
						$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message, 'errors' => ['usuario' => 'Su cuenta no tiene el privilegio']], 'status' => 422, 'route' => route('client.login'), 'message' => $message, 'type' => 'improper'];
						$data = $this->optimize($array);
					endif;
				else:
					$message = 'Contraseña incorrecta';
					$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message, 'errors' => ['password' => 'Su contraseña es incorrecta']], 'status' => 422, 'route' => route('client.login'), 'message' => $message, 'type' => 'improper'];
					$data = $this->optimize($array);
				endif;
			else:
				$message = 'El usuario no existe';
				$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message, 'errors' => ['usuario' => 'Su cuenta no existe']], 'status' => 422, 'route' => route('client.login'), 'message' => $message, 'type' => 'improper'];
				$data = $this->optimize($array);
			endif;
			return $data;
		}

		public function destroy(Request $request) {
			Auth::guard('client')->logout();
			$message = 'Session cerrada exitosamente';
			$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'errors' => null], 'status' => 200, 'route' => route('client.login'), 'message' => $message, 'type' => 'success'];
			$data = $this->optimize($array);
			return $data;
		}

		private function optimize($array) {
			session()->flash($array->type, $array->message);
			if ($array->request->ajax()):
				$data = response()->json($array->array, $array->status);
			else:
				$data = redirect($array->route);
			endif;
			return $data;
		}

	}
