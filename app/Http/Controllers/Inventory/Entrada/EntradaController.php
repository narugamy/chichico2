<?php

	namespace App\Http\Controllers\Inventory\Entrada;

	use App\Model\Concepto;
	use App\Model\Producto;
	use App\Model\Tipo;
	use Exception;
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use Illuminate\Support\Facades\Auth;
	use Illuminate\Support\Facades\DB;

	class EntradaController extends Controller {

		public function index() {
			$type = Tipo::where(['nombre' => 'entrada'])->first();
			$conceptos = Concepto::withTrashed()->where(['tipo_id' => $type->id])->get();
			return view('client.entry._index')->with(['conceptos' => $conceptos, 'title' => 'Modulo de orden de entradas']);
		}

		public function create(Request $request) {
			if (!$request->ajax()):
				$products = Producto::orderBy('nombre')->pluck('nombre', 'id');
				return view('client.entry.Create')->with(['title' => 'Registro de orden de entrada', 'products' => $products]);
			endif;
		}

		public function view(Request $request, $id) {
			if (!$request->ajax()):
				$concepto = Concepto::with('productos', 'tipo', 'usuario')->find($id);
				return view('client.entry.Show')->with(['title' => "Concepto N° $concepto->codigo", 'concepto' => $concepto]);
			else:
				return "No se permite por ajax";
			endif;
		}

		public function store(Request $request) {
			try {
				DB::beginTransaction();
				$concepto = new Concepto();
				$concepto->fill($request->all());
				$type = Tipo::where(['nombre' => 'entrada'])->first();
				$concepto->fill(['tipo_id' => $type->id, 'usuario_id' => Auth::guard('client')->user()->id, 'codigo' => $this->getcode()])->save();
				$elements = [];
				for($i = 0; $i < count($request->product_id); $i++):
					$product = Producto::find($request->product_id[$i]);
					$product->stock = (int)$product->stock + (int)$request->count[$i];
					$product->save();
					$elements[$request->product_id[$i]] = ['cantidad' => $request->count[$i]];
				endfor;
				$concepto->productos()->sync($elements);
				DB::commit();
				$message = "Registro exitoso";
				$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('client.entry.show', $concepto->id)], 'status' => 200, 'route' => route('client.entry.show', $concepto->id), 'message' => $message, 'type' => 'success'];
			} catch (Exception $e) {
				DB::rollBack();
				$message = "Ocurrio un error en el proceso";
				$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('client.entry.create'), 'message' => $message, 'type' => 'error'];
			}
			return $this->optimize($array);
		}

		public function destroy(Request $request, $id) {
			if ($request->ajax()):
				$concepto = Concepto::where(['estado' => 0])->with('productos', 'tipo', 'usuario')->find($id);
				if ($concepto):
					try {
						DB::beginTransaction();
						foreach($concepto->productos as $producto):
							$producto->stock -= (int)$producto->pivot->cantidad;
							$producto->save();
						endforeach;
						$concepto->forceDelete();
						DB::commit();
						$message = "Eliminacion exitosa";
						session()->flash('success', $message);
						$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('client.entry.index')], 200);
					} catch (Exception $e) {
						DB::rollBack();
						$message = "Ocurrio un error en el proceso";
						$data = response()->json(['resp' => false, 'message' => $message], 422);
					}
				else:
					$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, el orden de entrada no existe"], 422);
				endif;
			else:
				session()->flash('improper', "No intentes algo indebido");
				$data = redirect(route('client.entry.index'));
			endif;
			return $data;
		}

		public function status(Request $request, $id) {
			if ($request->ajax()):
				$concepto = Concepto::where(['estado' => 0])->find($id);
				if ($concepto):
					try {
						DB::beginTransaction();
						$concepto->fill(['estado' => 1])->save();
						DB::commit();
						$message = "Entrega exitosa";
						session()->flash('success', $message);
						$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('client.entry.index')], 200);
					} catch (Exception $e) {
						DB::rollBack();
						$data = response()->json(['resp' => false, 'message' => "Ocurrio un error en el proceso"], 422);
					}
				else:
					$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, el orden de entrada no existe"], 422);
				endif;
			else:
				session()->flash('improper', "No intentes algo indebido");
				$data = redirect(route('client.entry.index'));
			endif;
			return $data;
		}

		private function getcode() {
			$type = Tipo::where(['nombre' => 'entrada'])->first();
			$code = Concepto::where(['tipo_id' => $type->id])->max('codigo');
			return $code ? str_pad(((int)$code+1), 9, "0", STR_PAD_LEFT) : '000000001';
		}

		private function optimize($array) {
			session()->flash($array->type, $array->message);
			if ($array->request->ajax()):
				$data = response()->json($array->array, $array->status);
			else:
				$data = redirect($array->route);
			endif;
			return $data;
		}

	}
