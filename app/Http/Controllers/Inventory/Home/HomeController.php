<?php

	namespace App\Http\Controllers\Inventory\Home;

	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;

	class HomeController extends Controller {

		public function index() {
			return view('client.home._index')->with(['title' => 'Panel de Usuario', 'class_header' => 'page-container-bg-solid']);
		}

	}
