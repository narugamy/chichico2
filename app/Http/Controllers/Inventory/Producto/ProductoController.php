<?php

	namespace App\Http\Controllers\Inventory\Producto;

	use App\Model\Producto;
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;

	class ProductoController extends Controller {

		public function index() {
			$productos = Producto::withTrashed()->get();
			return view('client.product._index')->with(['products' => $productos, 'title' => 'Modulo de productos']);
		}

	}
