<?php

	namespace App\Http\Controllers\Admin\Indicador;

	use App\Model\Concepto;
	use App\Model\Producto;
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;

	class PrimerIndicadorController extends Controller {

		public function index() {
			return view('admin.primerindicador._index')->with(['title' => 'Rotación de Inventario']);
		}

		public function show(Request $request) {
			$conceptos = Concepto::where(['tipo_id' => 2, 'estado' => 1])->where('fecha', '>=', $request->fecha_ini)->where('fecha', '<=', $request->fecha_fin)->with('productos')->get();
			$salidas = 0;
			if (count($conceptos) > 0):
				foreach ($conceptos as $concepto):
					foreach ($concepto->productos as $producto):
						$salidas += (int)$producto->pivot->cantidad;
					endforeach;
				endforeach;
			endif;
			$stock = (int)Producto::sum('stock');
			return view('admin.primerindicador.Show')->with(['title' => 'Rotación de Inventario', 'salidas' => $salidas, 'stock' => $stock]);
		}

	}
