<?php

	namespace App\Http\Controllers\Admin\Indicador;

	use App\Model\Concepto;
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;

	class SegundoIndicadorController extends Controller {

		public function index() {
			return view('admin.segundoindicador._index')->with(['title' => 'Cumplimiento de Despacho']);
		}

		public function show(Request $request) {
			$conceptos = Concepto::where(['tipo_id' => 2, 'estado' => 1])->where('fecha', '>=', $request->fecha_ini)->where('fecha', '<=', $request->fecha_fin)->count();
			$total = Concepto::where(['tipo_id' => 2])->where('fecha', '>=', $request->fecha_ini)->where('fecha', '<=', $request->fecha_fin)->count();
			return view('admin.segundoindicador.Show')->with(['title' => 'Cumplimiento de Despacho', 'conceptos' => $conceptos, 'total' => $total]);
		}

	}
