<?php

	namespace App\Http\Controllers\Admin\Salida;

	use App\Model\Concepto;
	use App\Model\Producto;
	use App\Model\Tipo;
	use App\Model\Usuario;
	use Exception;
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use Illuminate\Support\Facades\DB;

	class SalidaController extends Controller {

		public function index() {
			$type = Tipo::where(['nombre' => 'salida'])->first();
			$conceptos = Concepto::withTrashed()->where(['tipo_id' => $type->id])->orderBy('codigo', 'desc')->get();
			return view('admin.output._index')->with(['conceptos' => $conceptos, 'title' => 'Modulo de orden de salida']);
		}

		public function create(Request $request) {
			if (!$request->ajax()):
				$users = Usuario::all();
				$products = Producto::where('stock','>', 0)->orderBy('nombre')->get();
				return view('admin.output.Create')->with(['title' => 'Registro de orden de salida', 'products' => $products, 'users' => $users]);
			endif;
		}

		public function view(Request $request, $id) {
			if (!$request->ajax()):
				$concepto = Concepto::with('productos', 'tipo', 'usuario')->find($id);
				return view('admin.output.Show')->with(['title' => "Concepto N° $concepto->codigo", 'concepto' => $concepto]);
			else:
				return "No se permite por ajax";
			endif;
		}

		public function store(Request $request) {
			try {
				DB::beginTransaction();
				$concepto = new Concepto();
				$concepto->fill($request->all());
				$type = Tipo::where(['nombre' => 'salida'])->first();
				$concepto->fill(['tipo_id' => $type->id, 'usuario_id' => $request->usuario_id, 'codigo' => $this->getcode(), 'estado' => 0])->save();
				$elements = [];
				$errors = null;
				for($i = 0; $i < count($request->product_id); $i++):
					$product = Producto::find($request->product_id[$i]);
					if((int)$product->stock >= (int)$request->count[$i]):
						$product->stock = (int)$product->stock - (int)$request->count[$i];
						$product->save();
						$elements[$request->product_id[$i]] = ['cantidad' => $request->count[$i]];
					else:
						$errors .= "El producto: $product->nombre tiene $product->stock como maximo de stock<br>";
					endif;
				endfor;
				if(empty($errors)):
					$concepto->productos()->sync($elements);
					DB::commit();
					$message = "Registro exitoso";
					$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('admin.output.show', $concepto->id)], 'status' => 200, 'route' => route('admin.output.show', $concepto->id), 'message' => $message, 'type' => 'success'];
				else:
					DB::rollBack();
					$message = "Ocurrio un error en el proceso";
					$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message, 'errors' => ['errors' => $errors]], 'status' => 422, 'route' => route('admin.output.create'), 'message' => $message, 'type' => 'error'];
				endif;
			} catch (Exception $e) {
				DB::rollBack();
				$message = "Ocurrio un error en el proceso";
				$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('admin.output.create'), 'message' => $message, 'type' => 'error'];
			}
			return $this->optimize($array);
		}

		public function destroy(Request $request, $id) {
			if ($request->ajax()):
				$concepto = Concepto::where(['estado' => 0])->with('productos', 'tipo', 'usuario')->find($id);
				if ($concepto):
					try {
						DB::beginTransaction();
						foreach($concepto->productos as $producto):
							$producto->stock += (int)$producto->pivot->cantidad;
							$producto->save();
						endforeach;
						$concepto->forceDelete();
						DB::commit();
						$message = "Eliminacion exitosa";
						session()->flash('success', $message);
						$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('admin.output.index')], 200);
					} catch (Exception $e) {
						DB::rollBack();
						$message = "Ocurrio un error en el proceso";
						$data = response()->json(['resp' => false, 'message' => $message], 422);
					}
				else:
					$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, el conceptoo no existe"], 422);
				endif;
			else:
				session()->flash('improper', "No intentes algo indebido");
				$data = redirect(route('admin.concepto.index'));
			endif;
			return $data;
		}

		public function status(Request $request, $id) {
			if ($request->ajax()):
				$concepto = Concepto::where(['estado' => 0])->find($id);
				if ($concepto):
					try {
						DB::beginTransaction();
						$concepto->fill(['estado' => 1])->save();
						DB::commit();
						$message = "Entrega exitosa";
						session()->flash('success', $message);
						$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('admin.output.index')], 200);
					} catch (Exception $e) {
						DB::rollBack();
						$data = response()->json(['resp' => false, 'message' => "Ocurrio un error en el proceso"], 422);
					}
				else:
					$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, el orden de salida no existe"], 422);
				endif;
			else:
				session()->flash('improper', "No intentes algo indebido");
				$data = redirect(route('admin.output.index'));
			endif;
			return $data;
		}

		private function getcode() {
			$type = Tipo::where(['nombre' => 'salida'])->first();
			$code = Concepto::where(['tipo_id' => $type->id])->max('codigo');
			return $code ? str_pad(((int)$code+1), 9, "0", STR_PAD_LEFT) : '000000001';
		}

		private function optimize($array) {
			session()->flash($array->type, $array->message);
			if ($array->request->ajax()):
				$data = response()->json($array->array, $array->status);
			else:
				$data = redirect($array->route);
			endif;
			return $data;
		}

	}
