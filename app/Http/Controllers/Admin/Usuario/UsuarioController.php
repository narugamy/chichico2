<?php

	namespace App\Http\Controllers\Admin\Usuario;

	use App\Model\Rol;
	use App\Model\Usuario;
	use Exception;
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use Illuminate\Support\Facades\DB;

	class UsuarioController extends Controller {

		public function index() {
			$users = Usuario::withTrashed()->with('rol')->get();
			return view('admin.user._index')->with(['title' => 'Panel de usuarios', 'class_header' => 'page-container-bg-solid', 'users' => $users]);
		}

		public function create(Request $request) {
			$roles = Rol::orderBy('nombre')->pluck('nombre', 'id');
			if ($request->ajax()):
				$data = view('admin.user._Create');
			else:
				$data = view('admin.user.Create')->with(['title' => 'Registro de usuario']);
			endif;
			return $data->with(['roles' => $roles]);
		}

		public function store(Request $request) {
			try {
				DB::beginTransaction();
				$user = new Usuario($request->all());
				$user->password = bcrypt($user->password);
				$user->save();
				DB::commit();
				$message = "Registro exitoso";
				$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('admin.user.index')], 'status' => 200, 'route' => route('admin.user.index'), 'message' => $message, 'type' => 'success'];
				$data = $this->optimize($array);
			} catch (Exception $e) {
				DB::rollBack();
				$message = "Ocurrio un error en el proceso";
				$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('admin.user.create'), 'message' => $message, 'type' => 'error'];
				$data = $this->optimize($array);
			}
			return $data;
		}

		public function show(Request $request, $usuario) {
			$user = Usuario::where(['usuario' => $usuario])->first();
			if ($user):
				$roles = Rol::orderBy('nombre')->pluck('nombre', 'id');
				if ($request->ajax()):
					$data = view('admin.user._Update');
				else:
					$data = view('admin.user.Update')->with(['title' => "Actualización del usuario: $user->full_name()"]);
				endif;
				$data->with(['user' => $user, 'roles' => $roles]);
			else:
				if ($request->ajax()):
					$data = "Usuario no existente";
				else:
					session()->flash('improper', "No intentes algo indebido, el usuario no existe");
					$data = redirect(route('admin.user.index'));
				endif;
			endif;
			return $data;
		}

		public function update(Request $request, $usuario) {
			$user = Usuario::where(['usuario' => $usuario])->first();
			if ($user):
				try {
					DB::beginTransaction();
					$password = $user->password;
					$user->fill($request->all());
					$user->password = (!empty($user->password)) ? bcrypt($request->password) : $password;
					$user->save();
					DB::commit();
					$message = "Actualización exitosa";
					$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('admin.user.index')], 'status' => 200, 'route' => route('admin.user.index'), 'message' => $message, 'type' => 'success'];
					$data = $this->optimize($array);
				} catch (Exception $e) {
					DB::rollBack();
					$message = "Ocurrio un error en el proceso";
					$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('admin.user.update', $user->usuario), 'message' => $message, 'type' => 'error'];
					$data = $this->optimize($array);
				}
			else:
				$message = "No intentes algo indebido, el usuario no existe";
				$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('admin.user.index'), 'message' => $message, 'type' => 'improper'];
				$data = $this->optimize($array);
			endif;
			return $data;
		}

		public function delete(Request $request, $usuario) {
			if ($request->ajax()):
				$user = Usuario::withTrashed()->where(['usuario' => $usuario])->first();
				if ($user):
					try {
						DB::beginTransaction();
						if ($user->deleted_at):
							$user->restore();
							$message = "Restauración exitosa";
						else:
							$user->delete();
							$message = "Deshabilitación exitosa";
						endif;
						DB::commit();
						session()->flash('success', $message);
						$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('admin.user.index')], 200);
					} catch (Exception $e) {
						DB::rollBack();
						$data = response()->json(['resp' => false, 'message' => "Ocurrio un error en el proceso"], 422);
					}
				else:
					$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, el usuario no existe"], 422);
				endif;
			else:
				session()->flash('improper', "No intentes algo indebido");
				$data = redirect(route('admin.user.index'));
			endif;
			return $data;
		}

		public function destroy(Request $request, $usuario) {
			if ($request->ajax()):
				$user = Usuario::withTrashed()->where(['usuario' => $usuario])->first();
				if ($user):
					try {
						DB::beginTransaction();
						$user->forceDelete();
						DB::commit();
						$message = "Eliminación exitosa";
						session()->flash('success', $message);
						$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('admin.user.index')], 200);
					} catch (Exception $e) {
						DB::rollBack();
						$data = response()->json(['resp' => false, 'message' => "Ocurrio un error en el proceso"], 422);
					}
				else:
					$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, el usuario no existe"], 422);
				endif;
			else:
				session()->flash('improper', "No intentes algo indebido");
				$data = redirect(route('admin.user.index'));
			endif;
			return $data;
		}

		private function optimize($array) {
			session()->flash($array->type, $array->message);
			if ($array->request->ajax()):
				$data = response()->json($array->array, $array->status);
			else:
				$data = redirect($array->route);
			endif;
			return $data;
		}

	}
