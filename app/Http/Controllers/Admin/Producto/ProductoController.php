<?php

	namespace App\Http\Controllers\Admin\Producto;

	use App\Model\Producto;
	use Exception;
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use Illuminate\Support\Facades\DB;
	use Illuminate\Support\Str;

	class ProductoController extends Controller {

		public function index() {
			$productos = Producto::withTrashed()->get();
			return view('admin.product._index')->with(['products' => $productos, 'title' => 'Modulo de productos']);
		}

		public function create(Request $request) {
			if ($request->ajax()):
				return view('admin.product._Create');
			endif;
		}

		public function store(Request $request) {
			try {
				DB::beginTransaction();
				$product = new Producto();
				$product->fill($request->all());
				$product->slug = Str::slug(strtolower($product->nombre),'-','es');
				$product->save();
				DB::commit();
				$message = "Registro exitoso";
				$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('admin.product.index')], 'status' => 200, 'route' => route('admin.product.index'), 'message' => $message, 'type' => 'success'];
				$data = $this->optimize($array);
			} catch (Exception $e) {
				DB::rollBack();
				$message = "Ocurrio un error en el proceso";
				$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $e], 'status' => 422, 'route' => route('admin.product.create'), 'message' => $message, 'type' => 'error'];
				$data = $this->optimize($array);
			}
			return $data;
		}

		public function show(Request $request, $slug) {
			if ($request->ajax()):
				$product = Producto::where(['slug' => $slug])->first();
				if($product):
					return view('admin.product._Update')->with(['product' => $product]);
				else:
					return "<div class='alert alert-danger'> <strong>Error! el producto no existe</strong></div>";
				endif;
			endif;
		}

		public function update(Request $request, $slug) {
			$product = Producto::where(['slug' => $slug])->first();
			if ($product):
				try {
					DB::beginTransaction();
					$product->fill($request->all())->save();
					DB::commit();
					$message = "Actualizacion exitosa";
					$array = (object)['request' => $request, 'array' => ['resp' => true, 'message' => $message, 'url' => route('admin.product.index')], 'status' => 200, 'route' => route('admin.product.index'), 'message' => $message, 'type' => 'success'];
					$data = $this->optimize($array);
				} catch (Exception $e) {
					DB::rollBack();
					$message = "Ocurrio un error en el proceso";
					$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('admin.product.update'), 'message' => $message, 'type' => 'error'];
					$data = $this->optimize($array);
				}
			else:
				$message = "El producto no existe";
				$array = (object)['request' => $request, 'array' => ['resp' => false, 'message' => $message], 'status' => 422, 'route' => route('admin.product.index'), 'message' => $message, 'type' => 'error'];
				$data = $this->optimize($array);
			endif;
			return $data;
		}

		public function delete(Request $request, $slug) {
			if ($request->ajax()):
				$product = Producto::withTrashed()->where(['slug' => $slug])->first();
				if ($product):
					try {
						DB::beginTransaction();
						if ($product->deleted_at):
							$product->restore();
							$message = "Restauracion exitosa";
						else:
							$product->delete();
							$message = "Deshabilitación exitosa";
						endif;
						DB::commit();
						session()->flash('success', $message);
						$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('admin.product.index')], 200);
					} catch (Exception $e) {
						DB::rollBack();
						$data = response()->json(['resp' => false, 'message' => "Ocurrio un error en el proceso"], 422);
					}
				else:
					$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, el producto no existe"], 422);
				endif;
			else:
				session()->flash('improper', "No intentes algo indebido");
				$data = redirect(route('admin.product.index'));
			endif;
			return $data;
		}

		public function destroy(Request $request, $slug) {
			if ($request->ajax()):
				$product = Producto::where(['slug' => $slug])->first();
				if ($product):
					try {
						DB::beginTransaction();
						$product->forceDelete();
						DB::commit();
						$message = "Eliminacion exitosa";
						session()->flash('success', $message);
						$data = response()->json(['resp' => true, 'message' => $message, 'url' => route('admin.product.index')], 200);
					} catch (Exception $e) {
						DB::rollBack();
						$data = response()->json(['resp' => false, 'message' => "Ocurrio un error en el proceso"], 422);
					}
				else:
					$data = response()->json(['resp' => false, 'message' => "No intentes algo indebido, el producto no existe"], 422);
				endif;
			else:
				session()->flash('improper', "No intentes algo indebido");
				$data = redirect(route('admin.product.index'));
			endif;
			return $data;
		}

		private function optimize($array) {
			session()->flash($array->type, $array->message);
			if ($array->request->ajax()):
				$data = response()->json($array->array, $array->status);
			else:
				$data = redirect($array->route);
			endif;
			return $data;
		}

	}
