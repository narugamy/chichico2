<?php

	Route::get('/', function () {
		return view('welcome');
	});
	Route::namespace('Admin')->prefix('admin')->group(function () {
		Route::namespace('Auth')->group(function () {
			Route::middleware('guest:admin')->group(function () {
				Route::get('login', 'LoginController@showLoginForm')->name('admin.login');
				Route::post('login', 'LoginController@login');
			});
			Route::middleware('auth:admin')->group(function () {
				Route::get('logout', 'LoginController@destroy')->name('admin.logout');
			});
		});
		Route::middleware('auth:admin')->group(function () {
			Route::namespace('Home')->group(function () {
				Route::get('/', 'HomeController@index')->name('admin.index');
			});
			Route::namespace('Indicador')->group(function () {
				Route::prefix('primerindicador')->group(function () {
					Route::get('/', 'PrimerIndicadorController@index')->name('admin.primer.indicador');
					Route::post('/', 'PrimerIndicadorController@show');
				});
				Route::prefix('segundoindicador')->group(function () {
					Route::get('/', 'SegundoIndicadorController@index')->name('admin.segundo.indicador');
					Route::post('/', 'SegundoIndicadorController@show');
				});
			});
			Route::namespace('Producto')->prefix('producto')->group(function () {
				Route::get('/', 'ProductoController@index')->name('admin.product.index');
				Route::get('create', 'ProductoController@create')->name('admin.product.create');
				Route::post('create', 'ProductoController@store');
				Route::get('{product}', 'ProductoController@show')->name('admin.product.update');
				Route::put('{product}', 'ProductoController@update');
				Route::get('{product}/delete', 'ProductoController@delete')->name('admin.product.delete');
				Route::get('{product}/destroy', 'ProductoController@destroy')->name('admin.product.destroy');
			});
			Route::namespace('Usuario')->prefix('usuario')->group(function () {
				Route::get('/', 'UsuarioController@index')->name('admin.user.index');
				Route::get('create', 'UsuarioController@create')->name('admin.user.create');
				Route::post('create', 'UsuarioController@store');
				Route::get('{user}', 'UsuarioController@show')->name('admin.user.update');
				Route::put('{user}', 'UsuarioController@update');
				Route::get('{user}/delete', 'UsuarioController@delete')->name('admin.user.delete');
				Route::get('{user}/destroy', 'UsuarioController@destroy')->name('admin.user.destroy');
			});
			Route::namespace('Entrada')->prefix('entrada')->group(function () {
				Route::get('/', 'EntradaController@index')->name('admin.entry.index');
				Route::get('create', 'EntradaController@create')->name('admin.entry.create');
				Route::post('create', 'EntradaController@store');
				Route::get('{entry}/show', 'EntradaController@view')->name('admin.entry.show');
				Route::get('{entry}/status', 'EntradaController@status')->name('admin.entry.status');
				Route::get('{entry}/destroy', 'EntradaController@destroy')->name('admin.entry.destroy');
			});
			Route::namespace('Salida')->prefix('salida')->group(function () {
				Route::get('/', 'SalidaController@index')->name('admin.output.index');
				Route::get('create', 'SalidaController@create')->name('admin.output.create');
				Route::post('create', 'SalidaController@store');
				Route::get('{output}/show', 'SalidaController@view')->name('admin.output.show');
				Route::get('{output}/destroy', 'SalidaController@destroy')->name('admin.output.destroy');
				Route::get('{output}/status', 'SalidaController@status')->name('admin.output.status');
			});
		});
	});
	Route::namespace('Inventory')->prefix('client')->group(function () {
		Route::namespace('Auth')->group(function () {
			Route::middleware('guest:client')->group(function () {
				Route::get('login', 'LoginController@showLoginForm')->name('client.login');
				Route::post('login', 'LoginController@login');
			});
			Route::middleware('auth:client')->group(function () {
				Route::get('logout', 'LoginController@destroy')->name('client.logout');
			});
		});
		Route::middleware('auth:client')->group(function () {
			Route::namespace('Home')->group(function () {
				Route::get('/', 'HomeController@index')->name('client.index');
			});
			Route::namespace('Entrada')->prefix('entrada')->group(function () {
				Route::get('/', 'EntradaController@index')->name('client.entry.index');
				Route::get('create', 'EntradaController@create')->name('client.entry.create');
				Route::post('create', 'EntradaController@store');
				Route::get('{entry}/show', 'EntradaController@view')->name('client.entry.show');
				Route::get('{entry}/status', 'EntradaController@status')->name('client.entry.status');
				Route::get('{entry}/destroy', 'EntradaController@destroy')->name('client.entry.destroy');
			});
			Route::namespace('Salida')->prefix('salida')->group(function () {
				Route::get('/', 'SalidaController@index')->name('client.output.index');
				Route::get('create', 'SalidaController@create')->name('client.output.create');
				Route::post('create', 'SalidaController@store');
				Route::get('{output}/show', 'SalidaController@view')->name('client.output.show');
				Route::get('{output}/destroy', 'SalidaController@destroy')->name('client.output.destroy');
				Route::get('{output}/status', 'SalidaController@status')->name('client.output.status');
			});
			Route::namespace('Producto')->prefix('producto')->group(function () {
				Route::get('/', 'ProductoController@index')->name('client.product.index');
			});
		});
	});
