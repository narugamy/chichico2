let FormValidationMd = function () {
	
	let enableEvents = function () {
		$('[data-submit="form"]').on('click', function (e) {
			e.preventDefault();
			console.log(1);
			let formId = $(e.currentTarget).attr('href');
			$(formId).submit();
		});
	};
	
	let handleValidation1 = function () {
		let form = $('.form');
		form.validate({
			errorElement: 'span', //default input error message container
			errorClass: 'help-block help-block-error', // default input error message class
			focusInvalid: false, // do not focus the last invalid input
			ignore: "",
			highlight: function (element) {
				$(element).closest('.form-group').addClass('has-error');
			}, unhighlight: function (element) {
				$(element).closest('.form-group').removeClass('has-error');
			}, success: function (label) {
				label.closest('.form-group').removeClass('has-error');
			}
		});
		jQuery.extend(jQuery.validator.messages, {
			required: "Este campo es obligatorio.",
			remote: "Por favor, rellena este campo.",
			email: "Por favor, escribe una dirección de correo válida",
			url: "Por favor, escribe una URL válida.",
			date: "Por favor, escribe una fecha válida.",
			dateISO: "Por favor, escribe una fecha (ISO) válida.",
			number: "Por favor, escribe un número entero válido.",
			digits: "Por favor, escribe sólo dígitos.",
			creditcard: "Por favor, escribe un número de tarjeta válido.",
			equalTo: "Por favor, escribe el mismo valor de nuevo.",
			accept: "Por favor, escribe un valor con una extensión aceptada.",
			maxlength: jQuery.validator.format("Por favor, no escribas más de {0} caracteres."),
			minlength: jQuery.validator.format("Por favor, no escribas menos de {0} caracteres."),
			rangelength: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
			range: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1}."),
			max: jQuery.validator.format("Por favor, escribe un valor menor o igual a {0}."),
			min: jQuery.validator.format("Por favor, escribe un valor mayor o igual a {0}.")
		});
	};
	return {
		init: function () {
			enableEvents();
			handleValidation1();
		}
	};
}();
jQuery(document).ready(function () {
	FormValidationMd.init();
});