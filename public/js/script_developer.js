$(document).ready(function () {
	let domain = window.location.origin, hash = window.location.pathname.split("/"), base, hijo, padre, base_url, formulario;
	if (hash.length > 2) {
		base_url = `${domain}/${hash[1]}/${hash[2]}`;
	} else {
		base_url = `${domain}/${hash[1]}`;
	}
	base = document.querySelector(`a.nav-link[href='${base_url}'`);
	if (base !== null) {
		hijo = base.parentNode;
		padre = hijo.parentNode.parentNode;
		hijo.classList.add('active');
		if (padre.classList.contains("nav-item")) {
			padre.classList.add('active');
			handleSidebarAndContentHeight();
		}
	}
	validad();
	if ($(document).find('.table').length > 0) {
		Create_Datatable('.table');
	}
	if ($(document).find('.form-submit').length > 0) {
		formulario = validation('.form-submit');
	} else if ($(document).find('.form-file').length > 0) {
		formulario = validation('.form-file');
	} else if ($(document).find('.form-assistance').length > 0) {
		formulario = validation('.form-file');
	} else if ($(document).find('.form-login').length > 0) {
		formulario = validation('.form-login');
	}
	if ($(document).find('.form-wizzard').length > 0) {
		wizzard('.wizzard', formulario);
	}
	$(document).on("submit", ".form-file", function () {
		let element = $(this), data = new FormData(this), url = $(this).attr("action");
		event.preventDefault();
		swal({
			title: "¿Esta seguro de realizar el proceso?",
			type: "info",
			cancelButtonText: "Cancelar",
			confirmButtonText: "Aceptar",
			showCancelButton: true,
			closeOnConfirm: false,
			showLoaderOnConfirm: true
		}, function () {
			element.find("[id*='-error']").remove();
			element.find(".has-error").removeClass("has-error");
			$.ajax({
					url: url,
					type: "POST",
					data: data,
					dataType: "json",
					async: false,
					cache: false,
					contentType: false,
					processData: false
				})
				.done(function (data) {
					swal({
						title: "Exito", text: `${data.message}`, timer: 1000, showConfirmButton: false, type: "success"
					});
					setTimeout(function () {
						window.location.href = data.url;
					}, 1000);
				})
				.fail(function (error) {
					failure(error, element);
				});
		});
	});
	$(document).on('click', '.btn-ajax', function () {
		let url = $(this).attr('data-url');
		$.get(url, null, null, 'html')
			.done(function (view) {
				$(document).find('.modal-body').html(view);
				$(document).find('#ajax').modal('show');
				let element;
				if ($(document).find('.form-submit').length > 0) {
					element = '.form-submit';
				} else {
					element = '.form-file';
				}
				let form = validation(element);
				wizzard('.wizzard', form);
				validad();
			})
			.fail(function (data) {
				if (data.status === 403) {
					swal({
						title: "Error",
						text: `Usted no tiene autorizacion para visualizar esta pagina`,
						timer: 2000,
						showConfirmButton: false,
						type: "error"
					});
				} else if (data.status === 422) {
					alert('No intentes algo indevido');
				}
			});
	});
	$(document).on('submit', '.form-login', function () {
		let url = $(this).attr('action'), container = $(this), data = $(this).serialize();
		event.preventDefault();
		swal({
			title: "Verificando...",
			text: "verificación de la información...",
			showConfirmButton: false,
			type: "info"
		});
		$(document).find("[id*='-error']").remove();
		$(document).find('.has-error').removeClass('has-error');
		$.post(url, data, null, 'json')
			.done(function (data) {
				swal({
					title: "Exito", text: `${data.message}`, timer: 1000, showConfirmButton: false, type: "success"
				});
				setTimeout(function () {
					window.location.href = data.url;
				}, 1000);
			})
			.fail(function (error) {
				failure(error, container);
			});
	});
	$(document).on('submit', '.form-submit', function (event) {
		let url = $(this).attr('action'), container = $(this), data = $(this).serialize();
		event.preventDefault();
		swal({
			title: "Desea enviar el formulario",
			confirmButtonText: "Enviar",
			cancelButtonText: "Cancelar",
			showCancelButton: true,
			closeOnConfirm: false,
			showLoaderOnConfirm: true
		}, function () {
			$(document).find("[id*='-error']").remove();
			$(document).find('.has-error').removeClass('has-error');
			$.post(url, data, null, 'json')
				.done(function (data) {
					swal({
						title: "Exito", text: `${data.message}`, timer: 1000, showConfirmButton: false, type: "success"
					});
					setTimeout(function () {
						window.location.href = data.url;
					}, 1000);
				})
				.fail(function (error) {
					failure(error, container);
				});
		});
	});
	$(document).on('click', '.btn-destroy', function () {
		let url = $(this).attr('data-url'), message = $(this).attr('data-message');
		swal({
			title: message,
			type: "info",
			cancelButtonText: "Cancelar",
			confirmButtonText: "Aceptar",
			showCancelButton: true,
			closeOnConfirm: false,
			showLoaderOnConfirm: true
		}, function () {
			$.get(url, null, null, 'json')
				.done(function (data) {
					swal({
						type: "success", title: data.message, showConfirmButton: false
					});
					setTimeout(function () {
						window.location.href = data.url;
					}, 1000);
				})
				.fail(function (error) {
					failure(error);
				});
		});
	});

	function mistakes(data, container) {
		$(container).find("[id*='-error']").remove();
		$(container).find('.has-error').removeClass('has-error');
		$.each(data, function (i, val) {
			if (val !== "") {
				container.find(`#${i}`).closest('.form-group').addClass('has-error');
				container.find(`#${i}`).parent().append(`<span id="${i}-error" class="help-block">${val}</span>`);
			}
		});
	}

	function validation(elements) {
		$('[data-submit="form"]').on('click', function (e) {
			e.preventDefault();
			let formId = $(e.currentTarget).attr('href');
			$(formId).submit();
		});
		let form = $(document).find(elements);
		form.validate({
			doNotHideMessage: true,
			errorElement: 'span',
			errorClass: 'help-block help-block-error',
			focusInvalid: false,
			highlight: function (element) {
				$(document).find(element).closest('.form-group').removeClass('has-success').addClass('has-error');
			},
			unhighlight: function (element) {
				$(document).find(element).closest('.form-group').removeClass('has-error').addClass('has-success');
			},
			success: function (label) {
				$(document).find(label).remove();
				$(document).find(label).closest('.form-group').removeClass('has-error');
			}
		});
		$.extend($.validator.messages, {
			required: "campo obligatorio.",
			remote: "rellena este campo.",
			email: "correo no válido",
			url: "URL no válida.",
			date: "fecha no valida.",
			dateISO: "fecha (ISO) inválida.",
			number: "numero no valido.",
			digits: "sólo dígitos.",
			creditcard: "número de tarjeta inválido.",
			equalTo: "el valor no es el mismo.",
			accept: "extensión invalida.",
			maxlength: $.validator.format("max. {0} caracteres."),
			minlength: $.validator.format("min. {0} caracteres."),
			rangelength: $.validator.format("entre {0} y {1} caracteres."),
			range: $.validator.format("min. {0}, max. {1}."),
			max: $.validator.format("max. {0}."),
			min: $.validator.format("min. {0}.")
		});
		return form;
	}

	function Create_Datatable(element) {
		let t = $(document).find(`${element}`);
		t.DataTable({
			order: [[t.find('th.order-now').index(), 'desc']],
			"aLengthMenu": [[5, 10, 15, -1], [5, 10, 15, 'All']], "language": {
				"buttons": {
					copyTitle: 'Copiado en porta papeles', copySuccess: {
						_: '%d lineas copiadas', 1: 'una linea copiada'
					}
				},
				"aria": {
					"sortAscending": ": Actilet para ordenar la columna de manera ascendente",
					"sortDescending": ": Actilet para ordenar la columna de manera descendente"
				},
				"infoFiltered": "(filtrado  de un total de _MAX_ registros)",
				"lengthMenu": `<span class="seperator"></span>Mostrar _MENU_ registros`,
				"sProcessing": "Procesando...",
				"info": `<span class="seperator"></span>Mostrando registros del _START_ al _END_`,
				"infoEmpty": "Mostrando registros del 0 al 0",
				"emptyTable": "Ningún dato disponible en esta tabla",
				"search": '<i class="fa fa-search"></i>',
				"paginate": {
					"previous": '<i class="fa fa-angle-left"></i>', "next": '<i class="fa fa-angle-right"></i>'
				},
				"zeroRecords": "No se encontraron resultados"
			}, "dom": "Bfrtip", buttons: [{
				extend: 'print',
				className: 'dt-button btn dark',
				text: `<i class="fa fa-print"></i>`,
				exportOptions: {columns: ':not(.acciones)'}
			}, {
				extend: 'copy',
				className: 'dt-button btn red',
				text: `<i class="fa fa-clipboard"></i>`,
				exportOptions: {columns: ':not(.acciones)'}
			}, {
				extend: 'pdf',
				className: 'dt-button btn blue',
				text: `<i class="fa fa-file-pdf-o"></i>`,
				defaultStyle: {alignment: "center"},
				exportOptions: {columns: ':not(.acciones)'},
				orientation: 'landscape',
				customize: function (doc) {
					let colCount = [];
					t.find("tbody tr:first-child td:not(.row-acction)").each(function () {
						if ($(this).attr('colspan')) {
							for (let i = 0; i < $(this).attr('colspan'); i++) {
								colCount.push('*');
							}
						} else {
							colCount.push('*');
						}
					});
					doc.styles.tableBodyOdd = {alignment: "center"};
					doc.styles.tableBodyEven = {alignment: "center"};
					doc.content[1].table.widths = '*';
				}
			}, {
				extend: 'excel',
				className: 'dt-button btn yellow',
				text: `<i class="fa fa-file-excel-o"></i>`,
				exportOptions: {columns: ":not(.acciones)"},
			}, {
				extend: 'colvis', className: 'dt-button btn purple', text: `<i class="fa fa-list-ul"></i>`
			}], responsive: true
		});
		$(document).find(`${element} tbody`).on('click', 'tr', function () {
			$(this).toggleClass('selected');
		});
	}

	function wizzard(element, form) {
		let handleTitle = function (tab, navigation, index) {
			let total = navigation.find('li').length;
			let current = index + 1;
			$('.step-title', $(document).find(`${element}`)).text('Step ' + (index + 1) + ' of ' + total);
			$('li', $(document).find(`${element}`)).removeClass("done");
			let li_list = navigation.find('li');
			for (let i = 0; i < index; i++) {
				$(li_list[i]).addClass("done");
			}
			if (current < 2) {
				$(document).find('.button-previous').hide();
			} else {
				$(document).find('.button-previous').show();
			}
			if (current >= total) {
				$(document).find('.button-next').hide();
			} else {
				$(document).find('.button-next').show();
			}
		};
		$(document).find(`${element}`).bootstrapWizard({
			'nextSelector': '.button-next',
			'previousSelector': '.button-previous',
			onTabClick: function (tab, navigation, index, clickedIndex) {
				if (form.valid() === false) {
					return false;
				}
				handleTitle(tab, navigation, clickedIndex);
			},
			onNext: function (tab, navigation, index) {
				if (form.valid() === false) {
					return false;
				}
				handleTitle(tab, navigation, index);
			},
			onPrevious: function (tab, navigation, index) {
				handleTitle(tab, navigation, index);
			},
			onTabShow: function (tab, navigation, index) {
				let total = navigation.find('li').length;
				let current = index + 1;
				let $percent = (current / total) * 100;
				$(document).find(`${element}`).find('.progress-bar').css({
					width: $percent + '%'
				});
				if ($(document).find('.button-previous').length > 0) {
					if (current < 2) {
						$(document).find('.button-previous').hide();
					}
				}
			}
		});
	}

	function handleSidebarAndContentHeight() {
		let resBreakpointMd = App.getResponsiveBreakpoint('md'), content = $(document).find('.page-content'),
			sidebar = $(document).find('.page-sidebar'), body = $('body'), height;
		if (body.hasClass("page-footer-fixed") === true && body.hasClass("page-sidebar-fixed") === false) {
			let available_height = App.getViewPort().height - $(document).find('.page-footer').outerHeight() - $(document).find('.page-header').outerHeight(),
				sidebar_height = sidebar.outerHeight();
			if (sidebar_height > available_height) {
				available_height = sidebar_height + $(document).find('.page-footer').outerHeight();
			}
			if (content.height() < available_height) {
				content.css('min-height', available_height);
			}
		} else {
			if (body.hasClass('page-sidebar-fixed')) {
				height = _calculateFixedSidebarViewportHeight();
				if (body.hasClass('page-footer-fixed') === false) {
					height = height - $(document).find('.page-footer').outerHeight();
				}
			} else {
				let headerHeight = $(document).find('.page-header').outerHeight(), footerHeight = $(document).find('.page-footer').outerHeight();
				if (App.getViewPort().width < resBreakpointMd) {
					height = App.getViewPort().height - headerHeight - footerHeight;
				} else {
					height = sidebar.height() + 20;
				}
				if ((height + headerHeight + footerHeight) <= App.getViewPort().height) {
					height = App.getViewPort().height - headerHeight - footerHeight;
				}
			}
			content.css('min-height', height);
		}
	}

	function _calculateFixedSidebarViewportHeight() {
		let sidebarHeight = App.getViewPort().height - $(document).find('.page-header').outerHeight(true);
		if ($('body').hasClass("page-footer-fixed")) {
			sidebarHeight = sidebarHeight - $(document).find('.page-footer').outerHeight();
		}
		return sidebarHeight;
	}

	function validad() {
		if ($(document).find('.text-number').length > 0) {
			$(document).on('input', '.text-number', function () {
				this.value = this.value.replace(/[^0-9]/g, '');
			});
		}
		if ($(document).find('.text-texter').length > 0) {
			$(document).on('input', '.text-texter', function () {
				this.value = this.value.replace(/[^a-zA-ZáéíóúüñÑ ]/, '');
			});
		}
		if ($(document).find('.select2').length > 0) {
			$(document).find('.select2').select2({
				placeholder: 'Seleccione una',
				width: '100%'
			});
		}
	}

	function failure(error, container = null) {
		let options = {};
		switch (error.status) {
			case 403:
				options = {
					title: "Error",
					text: `Usted no tiene autorizacion para visualizar esta pagina`,
					timer: 2000,
					showConfirmButton: false,
					type: "error"
				};
				break;
			case 422:
				options = {
					title: "Error",
					text: `${(error.responseJSON.message === "The given data was invalid.") ? 'Errores encontrados' : error.responseJSON.message}`,
					timer: 1000,
					showConfirmButton: false,
					type: "error"
				};
				break;
			case 429:
				options = {
					title: "Error",
					text: `${(error.responseJSON.message) ? error.responseJSON.message : 'Errores encontrados'}`,
					timer: 1000,
					showConfirmButton: false,
					type: "error"
				};
				break;
			default:
				options = {
					title: "Error",
					text: `Ocurrio un error en el proceso`,
					timer: 3000,
					showConfirmButton: false,
					type: "error"
				};
				break;
		}
		swal(options);
		if (error.status === 422 && container) {
			mistakes(error.responseJSON.errors, container);
		}
	}
});