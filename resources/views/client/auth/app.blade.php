<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="utf-8"/>
	<title>Login</title>
	<meta content="width=device-width, initial-scale=1" name="viewport"/>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
	<link href="{{ asset('css/app_admin_login.css') }}" rel="stylesheet" type="text/css"/>
	<!-- END HEAD -->
</head>
<body class=" login">
<!-- BEGIN LOGO -->
<div class="logo">
	<a href="">
		<img src="{{ asset('img/logo-big.png')}}" alt=""/> </a>
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content">
	{{ Form::open(['route' => 'client.login', 'class' => 'form-login form']) }}
	@if(session('success'))
		<div class="alert alert-success fade in">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			{{ session('success') }}
		</div>
	@elseif(session('improper'))
		<div class="alert alert-danger fade in">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			{{ session('improper') }}
		</div>
	@endif
	<h3 class="form-title font-green">Login</h3>
	<div class="form-group">
		<label class="control-label">Usuario</label>
		<input class="form-control form-control-solid placeholder-no-fix" type="text" placeholder="Usuario" name="usuario" id="usuario" data-rule-required="true" required>
	</div>
	<div class="form-group">
		<label class="control-label">Password</label>
		<input class="form-control form-control-solid placeholder-no-fix" type="password" placeholder="Password" name="password" id="password" data-rule-required="true">
	</div>
	<div class="form-actions">
		<button type="submit" class="btn green uppercase">Ingresar</button>
	</div>
	{{ Form::close() }}
</div>
<div class="copyright"> 2018 © KFGauss.</div>

<script src="{{ asset('js/app_admin.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/script_admin.min.js') }}" type="text/javascript"></script>
</body>
</html>
