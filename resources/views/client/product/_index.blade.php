@extends('client.layout.layout')
@section('styles')
	<link rel="stylesheet" href="{{ asset('css/style.css') }}">
@endsection
@section('container')
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<a href="{{ route('client.index') }}">Home</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<span>Productos</span>
				</li>
			</ul>
		</div>
		<h1 class="page-title"> {{ $title }}
		</h1>
		<div class="portlet green box">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-globe"></i>Lista
				</div>
				<div class="tools"></div>
			</div>
			<div class="portlet-body">
				<table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
					<thead>
					<tr>
						<th class="all order-now">N°</th>
						<th class="all">Nombre</th>
						<th class="none">Descripcion</th>
						<th>Stock</th>
					</tr>
					</thead>
					<tbody>
					@foreach($products as $product)
						<tr>
							<td>{{ $loop->iteration }}</td>
							<td>{{ $product->nombre }}</td>
							<td>{{ $product->descripcion }}</td>
							<td>{{ $product->stock }}</td>
						</tr>
					@endforeach
					</tbody>
				</table>
				<div class="clearfix"></div>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
@endsection
@section('scripts')
	<script src="{{ asset('js/datatables.min.js') }}" type="text/javascript"></script>
@endsection