@extends('client.layout.layout')
@section('styles')
	<link rel="stylesheet" href="{{ asset('css/style.css') }}">
@endsection
@section('container')
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<a href="{{ route('client.index') }}">Home</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<span>Entrada</span>
				</li>
			</ul>
			<div class="page-toolbar">
				<div class="btn-group pull-right">
					<a href="{{ route('client.entry.create') }}" class="btn green btn-sm btn-outline"> Crear</a>
				</div>
			</div>
		</div>
		<h1 class="page-title"> {{ $title }}
		</h1>
		@if(session('success'))
			<div class="alert alert-success fade in">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				{{ session('success') }}
			</div>
		@elseif(session('improper'))
			<div class="alert alert-danger fade in">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				{{ session('improper') }}
			</div>
		@endif
		<div class="portlet green box">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-globe"></i>Lista
				</div>
				<div class="tools"></div>
			</div>
			<div class="portlet-body">
				<table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
					<thead>
					<tr>
						<th class="all order-now">N°</th>
						<th class="all">Codigo</th>
						<th>Empresa</th>
						<th>Fecha</th>
						<th class="all acciones">Opciones</th>
					</tr>
					</thead>
					<tbody>
					@foreach($conceptos as $concepto)
						<tr>
							<td>{{ $loop->iteration }}</td>
							<td>{{ $concepto->codigo }}</td>
							<td>{{ $concepto->empresa }}</td>
							<td>{{ date('d-m-Y', strtotime($concepto->fecha)) }}</td>
							<td class="row-acction">
								<div class="btn-group">
									<button class="btn {{ ($concepto->deleted_at)? 'red':'blue' }} btn-xs btn-outline dropdown-toggle" data-toggle="dropdown">Acciones<i class="fa fa-angle-down"></i>
									</button>
									<ul class="dropdown-menu">
										<li>
											<a href="{{ route('client.entry.show', $concepto->id) }}"><i class="fa fa-pencil"></i> Mostrar</a>
										</li>
										@if((int)$concepto->estado === 0)
											<li>
												<a data-url="{{ route('client.output.destroy', $concepto->id) }}" data-message="Desea eliminar el orden de salida: {{ $concepto->codigo }}" class="btn-destroy"><i class="fa fa-trash"></i> Eliminar</a>
											</li>
											<li>
												<a data-url="{{ route('client.output.status', $concepto->id) }}" data-message="Desea cambiar a entregado el orden de salida: {{ $concepto->codigo }}" class="btn-destroy"><i class="fa fa-flag"></i> Entregado</a>
											</li>
										@endif
									</ul>
								</div>
							</td>
						</tr>
					@endforeach
					</tbody>
				</table>
				<div class="clearfix"></div>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
	<!-- AJAX -->
	<div class="modal fade" id="ajax" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-body">
				</div>
			</div>
		</div>
	</div>
@endsection
@section('scripts')
	<script src="{{ asset('js/datatables.min.js') }}" type="text/javascript"></script>
@endsection