@extends('client.layout.layout')
@section('styles')
	<link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css"/>
@endsection
@section('container')
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<a href="{{ route('client.index') }}">Home</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<a href="{{ route('client.output.index') }}">Salida</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<span>Mostrar</span>
				</li>
			</ul>
		</div>
		<!-- END PAGE BAR -->
		<!-- BEGIN PAGE TITLE-->
		<h1 class="page-title"> {{ $title }}
		</h1>
		<div class="form-wizzard">
			<h1>Documento de salida</h1>
			<div class="flex-4">
				<p class="no-margin">Receptor de la salida: {{ $concepto->usuario->full_name() }}</p>
				<p class="no-margin">Codigo: {{ $concepto->codigo }}</p>
				<p class="no-margin">Fecha de emision: {{ date('d-m-Y', strtotime($concepto->fecha)) }}</p>
				<p class="no-margin">Estado: {{ ((int)$concepto->estado === 0)? 'en proceso':'recibido' }}</p>
			</div>
			<div class="row">
				<div class="portlet-body">
					<table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
						<thead>
						<tr>
							<th class="all order-now">N°</th>
							<th class="all">Nombre</th>
							<th>Cantidad</th>
						</tr>
						</thead>
						<tbody>
						@foreach($concepto->productos as $product)
							<tr>
								<td>{{ $loop->iteration }}</td>
								<td>{{ $product->nombre }}</td>
								<td>{{ $product->pivot->cantidad }}</td>
							</tr>
						@endforeach
						</tbody>
					</table>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('scripts')
	<script src="{{ asset('js/datatables.min.js') }}" type="text/javascript"></script>
@endsection