<div class="portlet light bordered wizzard portlet-form">
	<div class="portlet-body form">
		{{ Form::open(['route' => 'client.output.create', 'class' => 'form-horizontal form-submit']) }}
		<div class="form-wizard">
			<div class="form-body">
				<ul class="nav nav-pills nav-justified steps">
					<li>
						<a href="#tab1" data-toggle="tab" class="step">
							<span class="number"> 1 </span>
							<span class="desc">
								<i class="fa fa-check"></i> Principal
							</span>
						</a>
					</li>
				</ul>
				<div id="bar" class="progress progress-striped" role="progressbar">
					<div class="progress-bar progress-bar-success active"></div>
				</div>
				<div class="tab-content portlet-body">
					<div class="tab-pane active" id="tab1">
						<div class="form-body">
							<div class="flex-3">
								<div class="form-group form-md-line-input form-md-floating-label">
									<select name="usuario_id" id="usuario_id" required data-rule-required="true" class="form-control edited">
										<option value="">Seleccionar uno</option>
										@foreach($users as $user)
											<option value="{{ $user->id }}">{{ $user->full_name() }}</option>
										@endforeach
									</select>
									<label for="usuario_id">Personal</label>
								</div>
								<div class="form-group form-md-line-input form-md-floating-label">
									<input type="date" class="form-control edited" name="fecha" id="fecha" data-rule-required="true" data-rule-date="true" required>
									<label for="fecha">Fecha</label>
								</div>
							</div>
							<div class="flex-3">
								<div class="form-group form-md-line-input form-md-floating-label">
									<select name="producto_id" id="producto_id" required data-rule-required="true" class="form-control edited select-element">
										<option value="">Seleccionar uno</option>
										@foreach($products as $product)
											<option value="{{ $product->id }}" data-max="{{ $product->stock }}">{{ $product->nombre }}</option>
										@endforeach
									</select>
									<label for="producto_id">Producto</label>
								</div>
								<div class="form-group form-md-line-input form-md-floating-label text-center">
									<button type="button" class="btn green button-add"> Agregar
										<i class="fa fa-check"></i></button>
								</div>
							</div>
							<table class="table table-striped table-bordered table-hover">
								<thead>
								<tr>
									<th class="all">Nombre</th>
									<th class="none">Cantidad</th>
									<th class="none">Max. aprox</th>
									<th class="all">Accion</th>
								</tr>
								</thead>
								<tbody class="table-elements">
								</tbody>
							</table>
							<div id="errors"></div>
							<div class="clearfix"></div>
							<div class="form-group form-md-line-input form-md-floating-label text-center">
								<button class="btn green button-submit"> Crear
									<i class="fa fa-check"></i>
								</button>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
		{{ Form::close() }}
	</div>
</div>