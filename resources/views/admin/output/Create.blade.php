@extends('admin.layout.layout')
@section('styles')
	<link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css"/>
@endsection
@section('container')
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<a href="{{ route('admin.index') }}">Home</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<a href="{{ route('admin.output.index') }}">Salida</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<span>Crear</span>
				</li>
			</ul>
		</div>
		<!-- END PAGE BAR -->
		<!-- BEGIN PAGE TITLE-->
		<h1 class="page-title"> {{ $title }}
		</h1>
		<div class="form-wizzard">
			@if(session('success'))
				<div class="alert alert-success fade in">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					{{ session('success') }}
				</div>
			@elseif(session('error'))
				<div class="alert alert-danger fade in">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					{{ session('error') }}
				</div>
			@endif
			@if(count($errors) > 0)
				<div class="alert alert-danger fade in">
					<ul>
						@foreach($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif
			@include('admin.output._Create')
		</div>
	</div>
@endsection
@section('scripts')
	<script type="application/javascript">
		$(document).ready(function () {
			$(document).on('click', '.button-add', function() {
				let item = $(document).find('#producto_id option:selected'), id = item.val(), name = item.text(), max = item.attr('data-max');
				if((parseInt(id) > 0)  && validate(id) === false){
					let object = `<tr><td>${ name }<input type="hidden" value="${id}" name="product_id[]"></td><td><input type="number" name="count[]" class="form-control" value="1" max="${max}" data-rule-max="${max}"></td><td>${max} unidades</td><td><button type="button" class="btn btn-primary btn-destroyer">Eliminar</button></td></tr>`;
					$(document).find('.table-elements').append(object);
				}
			});
			$(document).on('click', '.btn-destroyer', function () {
				$(this).parent().parent().remove();
			});
			function validate(id) {
				let value = false;
				$('input[name="product_id[]"]').each(function() {
					if(parseInt($(this).val()) === parseInt(id)){
						value = true;
					}
				});
				return value;
			}
		});
	</script>
@endsection