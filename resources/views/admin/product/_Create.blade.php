<div class="portlet light bordered wizzard portlet-form">
	<div class="portlet-body form">
		{{ Form::open(['route' => 'admin.product.create', 'class' => 'form-horizontal form-submit']) }}
		<div class="form-wizard">
			<div class="form-body">
				<ul class="nav nav-pills nav-justified steps">
					<li>
						<a href="#tab1" data-toggle="tab" class="step">
							<span class="number"> 1 </span>
							<span class="desc">
								<i class="fa fa-check"></i> Principal
							</span>
						</a>
					</li>
				</ul>
				<div id="bar" class="progress progress-striped" role="progressbar">
					<div class="progress-bar progress-bar-success active"></div>
				</div>
				<div class="tab-content portlet-body">
					<div class="tab-pane active" id="tab1">
						<div class="form-body">
							<div class="form-group form-md-line-input form-md-floating-label">
								<input type="text" class="form-control" name="nombre" id="nombre" data-rule-required="true" data-rule-minlength="3" data-rule-maxlength="191" minlength="3" maxlength="191" required>
								<label for="nombre">Nombre</label>
							</div>
							<div class="form-group form-md-line-input form-md-floating-label">
								<textarea class="form-control" name="descripcion" id="descripcion" data-rule-minlength="3" minlength="3"></textarea>
								<label for="descripcion">Descripcion</label>
							</div>
							<div class="form-group form-md-line-input form-md-floating-label">
								<input type="text" class="form-control text-number" name="stock" id="stock" data-rule-required="true" data-rule-minlength="1" data-rule-maxlength="5" minlength="1" maxlength="5" required value="0">
								<label for="stock">Stock</label>
							</div>
							<div class="form-group form-md-line-input form-md-floating-label text-center">
								<button class="btn green button-submit"> Crear
									<i class="fa fa-check"></i>
								</button>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
		{{ Form::close() }}
	</div>
</div>