<div class="portlet light bordered wizzard portlet-form">
	<div class="portlet-body form">
		{{ Form::open(['route' => ['admin.user.update', $user->usuario], 'class' => 'form-horizontal form-submit', 'method' => 'put']) }}
		<div class="form-wizard">
			<div class="form-body">
				<ul class="nav nav-pills nav-justified steps">
					<li>
						<a href="#tab1" data-toggle="tab" class="step">
							<span class="number"> 1 </span>
							<span class="desc">
								<i class="fa fa-check"></i> Principal
							</span>
						</a>
					</li>
				</ul>
				<div id="bar" class="progress progress-striped" role="progressbar">
					<div class="progress-bar progress-bar-success active"></div>
				</div>
				<div class="tab-content portlet-body">
					<div class="tab-pane active" id="tab1">
						<div class="form-body">
							<div class="form-group form-md-line-input form-md-floating-label">
								<input type="text" class="form-control text-texter edited" name="nombres" id="nombres" data-rule-required="true" data-rule-minlength="3" data-rule-maxlength="191" minlength="3" maxlength="191" required value="{{ $user->nombres }}">
								<label for="nombres">Nombres</label>
							</div>
							<div class="form-group form-md-line-input form-md-floating-label">
								<input type="text" class="form-control text-texter edited" name="apellidos" id="apellidos" data-rule-required="true" data-rule-minlength="3" data-rule-maxlength="191" minlength="3" maxlength="191" required value="{{ $user->apellidos }}">
								<label for="apellidos">Apellidos</label>
							</div>
							<div class="form-group form-md-line-input form-md-floating-label">
								<input type="text" class="form-control edited" name="usuario" id="usuario" data-rule-required="true" data-rule-minlength="5" data-rule-maxlength="191" minlength="5" maxlength="191" value="{{ $user->usuario }}" required>
								<label for="usuario">Usuario</label>
							</div>
							<div class="form-group form-md-line-input form-md-floating-label">
								<input type="password" class="form-control edited" name="password" id="password" data-rule-minlength="5" data-rule-maxlength="20" data-rule-password="true" minlength="5" maxlength="20">
								<label for="password">Contraseña</label>
							</div>
							<div class="form-group form-md-line-input form-md-floating-label">
								{{ Form::select('role_id', $roles, $user->rol->id, ['id' => 'role_id', 'required' => '', 'placeholder' => 'Seleccionar una', 'data-rule-required' => 'true', 'class' => 'form-control edited']) }}
								<label for="role_id">Rol</label>
							</div>
							<div class="form-group form-md-line-input form-md-floating-label text-center">
								<button class="btn green button-submit"> Actualizar
									<i class="fa fa-check"></i>
								</button>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
		{{ Form::close() }}
	</div>
</div>