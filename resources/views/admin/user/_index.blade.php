@extends('admin.layout.layout')
@section('styles')
	<link rel="stylesheet" href="{{ asset('css/style.css') }}">
@endsection
@section('container')
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<a href="{{ route('admin.index') }}">Home</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<span>Usuarios</span>
				</li>
			</ul>
			<div class="page-toolbar">
				<div class="btn-group pull-right">
					<a data-url="{{ route('admin.user.create') }}" class="btn green btn-sm btn-outline btn-ajax"> Crear</a>
				</div>
			</div>
		</div>
		<h1 class="page-title"> {{ $title }}
		</h1>
		@if(session('success'))
			<div class="alert alert-success fade in">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				{{ session('success') }}
			</div>
		@elseif(session('improper'))
			<div class="alert alert-danger fade in">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				{{ session('improper') }}
			</div>
		@endif
		<div class="portlet green box">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-globe"></i>Lista
				</div>
				<div class="tools"></div>
			</div>
			<div class="portlet-body">
				<table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
					<thead>
					<tr>
						<th class="all order-now">N°</th>
						<th class="all">Nombre</th>
						<th class="all">Rol</th>
						<th class="all acciones">Opciones</th>
					</tr>
					</thead>
					<tbody>
					@foreach($users as $user)
						<tr>
							<td>{{ $loop->iteration }}</td>
							<td>{{ $user->full_name() }}</td>
							<td>{{ $user->rol->nombre }}</td>
							<td class="row-acction">
								<div class="btn-group">
									<button class="btn {{ ($user->deleted_at)? 'red':'blue' }} btn-xs btn-outline dropdown-toggle" data-toggle="dropdown">Acciones<i class="fa fa-angle-down"></i>
									</button>
									<ul class="dropdown-menu">
										@if(!$user->deleted_at)
											<li>
												<a data-url="{{ route('admin.user.update', $user->usuario) }}" class="btn-ajax"><i class="fa fa-pencil"></i> Mostrar</a>
											</li>
											<li>
												<a data-url="{{ route('admin.user.delete', $user->usuario) }}" data-message="Desea deshabilitar al usuario: {{ $user->full_name() }}" class="btn-destroy"><i class="fa fa-low-vision"></i> Deshabilitar</a>
											</li>
											<li>
												<a data-url="{{ route('admin.user.destroy',$user->usuario) }}" data-message="Desea eliminar al usuario: {{ $user->full_name() }}" class="btn-destroy"><i class="fa fa-trash"></i> Eliminar</a>
											</li>
										@else
											<li>
												<a data-url="{{ route('admin.user.delete',$user->usuario) }}" data-message="Desea restaurar al usuario: {{ $user->full_name() }}" class="btn-destroy"><i class="fa fa-recycle"></i> Restaurar</a>
											</li>
										@endif
									</ul>
								</div>
							</td>
						</tr>
					@endforeach
					</tbody>
				</table>
				<div class="clearfix"></div>
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
	<!-- AJAX -->
	<div class="modal fade" id="ajax" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-body">
				</div>
			</div>
		</div>
	</div>
@endsection
@section('scripts')
	<script src="{{ asset('js/datatables.min.js') }}" type="text/javascript"></script>
@endsection