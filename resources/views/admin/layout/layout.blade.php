<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="utf-8"/>
	<title>{{ $title }}</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1" name="viewport"/>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
	<link href="{{ asset('css/app_admin.min.css') }}" rel="stylesheet" type="text/css"/>
	@yield('styles')
</head>
<body class="page-header-fixed page-sidebar-closed-hide-logo{{!empty($class_header) ? " $class_header ":'   '}}page-content-white page-md">
<div class="page-wrapper">
	<div class="page-header navbar navbar-fixed-top">
		<div class="page-header-inner ">
			<div class="page-logo">
				<a href="{{ route('admin.index') }}">
					<img src="{{ asset('img/logo.png') }}" alt="logo" class="logo-default"/> </a>
				<div class="menu-toggler sidebar-toggler">
					<span></span>
				</div>
			</div>
			<a href="#" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
				<span></span>
			</a>
			<div class="top-menu">
				<ul class="nav navbar-nav pull-right">
					<li class="dropdown dropdown-user">
						<a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
							<img alt="" class="img-circle" src="{{ asset('img/avatar3_small.jpg') }}"/>
							<span class="username username-hide-on-mobile"> {{ Auth::user()->full_name() }} </span>
							<i class="fa fa-angle-down"></i>
						</a>
						<ul class="dropdown-menu dropdown-menu-default">
							<li>
								<a href="{{ route('admin.logout') }}"><i class="icon-key"></i> Salir </a>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="page-container">
		<div class="page-sidebar-wrapper">
			<div class="page-sidebar navbar-collapse collapse">
				<ul class="page-sidebar-menu page-header-fixed page-sidebar-menu-light page-sidebar-menu-hover-submenu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
					<li class="sidebar-toggler-wrapper hide">
						<div class="sidebar-toggler">
							<span></span>
						</div>
					</li>
					<li class="sidebar-search-wrapper">
						<div class="sidebar-search">
							<a class="remove">
								<i class="icon-close"></i>
							</a>
						</div>
					</li>
					<li class="nav-item start">
						<a class="nav-link nav-toggle">
							<i class="icon-home"></i>
							<span class="title">Inicio</span>
							<span class="selected"></span>
							<span class="arrow open"></span>
						</a>
						<ul class="sub-menu">
							<li class="nav-item start">
								<a href="{{ route('admin.index') }}" class="nav-link">
									<i class="icon-bar-chart"></i>
									<span class="title">Inicio</span>
								</a>
							</li>
						</ul>
					</li>
					<li class="nav-item">
						<a class="nav-link nav-toggle">
							<i class="icon-bar-chart"></i>
							<span class="title">Panel Principal</span>
							<span class="selected"></span>
							<span class="arrow open"></span>
						</a>
						<ul class="sub-menu">
							<li class="nav-item">
								<a href="{{ route('admin.product.index') }}" class="nav-link">
									<i class="icon-present"></i>
									<span class="title"> Producto</span>
								</a>
							</li>
							<li class="nav-item">
								<a href="{{ route('admin.user.index') }}" class="nav-link">
									<i class="icon-user-follow"></i>
									<span class="title"> Usuarios</span>
								</a>
							</li>
							<li class="nav-item">
								<a href="{{ route('admin.entry.index') }}" class="nav-link">
									<i class="glyphicon glyphicon-indent-left"></i>
									<span class="title"> Entrada</span>
								</a>
							</li>
							<li class="nav-item">
								<a href="{{ route('admin.output.index') }}" class="nav-link">
									<i class="glyphicon glyphicon-indent-right"></i>
									<span class="title"> Salida</span>
								</a>
							</li>
						</ul>
					</li>
					<li class="nav-item">
						<a class="nav-link nav-toggle">
							<i class="icon-home"></i>
							<span class="title">Indicadores</span>
							<span class="selected"></span>
							<span class="arrow open"></span>
						</a>
						<ul class="sub-menu">
							<li class="nav-item">
								<a href="{{ route('admin.primer.indicador') }}" class="nav-link">
									<i class="icon-bar-chart"></i>
									<span class="title">Primer Indicador</span>
								</a>
							</li>
							<li class="nav-item">
								<a href="{{ route('admin.segundo.indicador') }}" class="nav-link">
									<i class="icon-bar-chart"></i>
									<span class="title">Segundo Indicador</span>
								</a>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
		<div class="page-content-wrapper">
			@yield('container')
		</div>
	</div>
	<div class="page-footer">
		<div class="page-footer-inner text-center"> 2018 © Desarrollo By
			<a target="_blank" href="http://programofacil.com">Giovanni CC</a>
		</div>
		<div class="scroll-to-top">
			<i class="icon-arrow-up"></i>
		</div>
	</div>
</div>
<script src="{{ asset('js/app_admin.min.js') }}" type="text/javascript"></script>
@yield('scripts')
<script src="{{ asset('js/script_admin.min.js') }}" type="text/javascript"></script>
</body>
</html>