<div class="col-sm-6">
	<h3><strong>Rotación de Inventarios (R)</strong></h3>
	<p>Unidades de salida (USA): {{ $salidas }}</p>
	<p>Unidades de Stock (UST): {{ $stock }}</p>
	@if($stock > 0)
		<p>USA/UST = {{ round(($salidas / $stock)*100,2) }}%</p>
	@else
		<p>USA/UST = {{ 0 }}%</p>
	@endif
	<hr>
</div>
<div style="display: flex;justify-content: center;margin-bottom: 2em;" class="col-sm-6">
	<canvas id="myChart" width="200" height="200" style="max-width: 400px;max-height: 400px"></canvas>
</div>
<script>
	$(document).ready(function () {
		chart();
	});
	function chart() {
		var ctx = document.getElementById("myChart").getContext("2d");
		let myChart = new Chart(ctx, {
			type: 'pie',
			data: {
				labels: ["Salida", "Stock"],
				datasets: [{
					label: '# of Votes',
					data: [parseInt({{ $salidas }}),parseInt({{ $stock }})],
					backgroundColor: [
						'rgba(255, 99, 132, 0.2)',
						'rgba(54, 162, 235, 0.2)',
					],
					borderColor: [
						'rgba(255,99,132,1)',
						'rgba(54, 162, 235, 1)',
					],
					borderWidth: 1
				}]
			},
			options: {
				scales: {
					yAxes: [{
						ticks: {
							beginAtZero:true
						}
					}]
				}
			}
		});
	}
</script>