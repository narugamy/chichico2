@extends('admin.layout.layout')
@section('styles')
	{{ Html::style('css/bootstrap-datepicker.min.css') }}
@endsection
@section('container')
	<div class="page-content">
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<a href="{{ route('admin.index') }}">Home</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<span>Segundo Indicador</span>
				</li>
			</ul>
		</div>
		<h1 class="page-title"> {{ $title }}
		</h1>
		@if(session('success'))
			<div class="alert alert-success fade in">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				{{ session('success') }}
			</div>
		@elseif(session('improper'))
			<div class="alert alert-danger fade in">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				{{ session('improper') }}
			</div>
		@endif
		<div class="row">
			<div class="col-md-12">
				<div class="portlet light profile-sidebar-portlet">
					<div class="profile-usertitle">
						{{ Form::open(['route' => 'admin.segundo.indicador', 'class' => 'form-horizontal form-indicator']) }}
						<span class="section">Reporte N° 1</span>
						<div class="input-group input-daterange">
							<input type="text" class="form-control" name="fecha_ini" readonly placeholder="Fecha inicio" required id="fecha_ini" data-rule-required="true">
							<div class="input-group-addon">a</div>
							<input type="text" class="form-control" name="fecha_fin" readonly placeholder="Fecha fin" required id="fecha_fin" data-rule-required="true">
						</div>
						<div class="clearfix"></div>
						<div class="form-group">
							<div class="col-md-6 col-md-offset-5">
								<button type="submit" class="btn btn-success">Mostrar</button>
							</div>
						</div>
						{{ Form::close() }}
					</div>
					<div class="profile-usermenu"></div>
				</div>
				<div class="indicadores">
				</div>
				<!-- END PROFILE CONTENT -->
			</div>
		</div>
	</div>
@endsection
@section('scripts')
	{{ Html::script('js/bootstrap-datepicker.min.js') }}
	{{ Html::script('js/bootstrap-datepicker.es.min.js') }}
	{{ Html::script("js/Chart.min.js") }}
			<script>
		$(document).ready(function () {
			if ($('.input-daterange').length > 0) {
				$('.input-daterange').datepicker({language: 'es', format: 'yyyy-mm-dd'});
			}
			$(document).on('submit', '.form-indicator', function (event) {
				let url = $(this).attr('action'), data = $(this).serialize();
				event.preventDefault();
				swal({
					title: "¿Desea realizar el proceso?",
					confirmButtonText: "Enviar",
					cancelButtonText: "Cancelar",
					showCancelButton: true,
					closeOnConfirm: false,
					showLoaderOnConfirm: true
				}, function () {
					$(document).find("[id*='-error']").remove();
					$(document).find('.has-error').removeClass('has-error');
					$.post(url, data, null, 'html')
						.done(function (view) {
							swal.close();
							$(document).find('.indicadores').html(view);
							console.log(view);
						})
						.fail(function (error) {
							swal({
								title: "Error",
								text: `${(error.responseJSON) ? error.responseJSON.message : 'Errores encontrados'}`,
								timer: 3000,
								showConfirmButton: false,
								type: "error"
							});
						});
				});
			});
		});
	</script>
@endsection