<?php

	use Illuminate\Support\Facades\Schema;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;

	class CreateProductoTable extends Migration {

		public function up() {
			Schema::create('producto', function (Blueprint $table) {
				$table->increments('id');
				$table->string('nombre')->unique();
				$table->text('descripcion');
				$table->integer('stock');
				$table->string('slug')->unique();
				$table->timestamps();
				$table->softDeletes();
			});
		}

		public function down() {
			Schema::dropIfExists('producto');
		}

	}
