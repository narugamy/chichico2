<?php

	use Illuminate\Support\Facades\Schema;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;

	class CreateUsuarioTable extends Migration {

		public function up() {
			Schema::create('usuario', function (Blueprint $table) {
				$table->increments('id');
				$table->integer('rol_id')->unsigned()->index();
				$table->foreign('rol_id')->references('id')->on('rol')->onDelete('cascade');
				$table->string('nombres');
				$table->string('apellidos');
				$table->string('usuario')->unique();
				$table->string('password');
				$table->rememberToken();
				$table->timestamps();
				$table->softDeletes();
			});
		}

		public function down() {
			Schema::dropIfExists('usuario');
		}
	}
