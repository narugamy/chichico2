<?php

	use Illuminate\Support\Facades\Schema;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;

	class CreateConceptoTable extends Migration {

		public function up() {
			Schema::create('concepto', function (Blueprint $table) {
				$table->increments('id');
				$table->integer('usuario_id')->unsigned()->nullable();
				$table->foreign('usuario_id')->references('id')->on('usuario')->onDelete('cascade');
				$table->integer('tipo_id')->unsigned();
				$table->foreign('tipo_id')->references('id')->on('tipo')->onDelete('cascade');
				$table->string('codigo', 11);
				$table->string('empresa')->nullable();
				$table->date('fecha');
				$table->string('estado', 1);
				$table->timestamps();
				$table->softDeletes();
			});
		}

		public function down() {
			Schema::dropIfExists('concepto');
		}

	}
