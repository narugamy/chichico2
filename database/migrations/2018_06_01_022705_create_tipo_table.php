<?php

	use Illuminate\Support\Facades\Schema;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;

	class CreateTipoTable extends Migration {

		public function up() {
			Schema::create('tipo', function (Blueprint $table) {
				$table->increments('id');
				$table->string('nombre')->unique();
				$table->string('slug')->unique();
				$table->timestamps();
				$table->softDeletes();
			});
		}

		public function down() {
			Schema::dropIfExists('tipo');
		}

	}
