<?php

	use Illuminate\Support\Facades\Schema;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;

	class CreateConceptoProductoTable extends Migration {

		public function up() {
			Schema::create('concepto_producto', function (Blueprint $table) {
				$table->increments('id');
				$table->integer('concepto_id')->unsigned()->index();
				$table->foreign('concepto_id')->references('id')->on('concepto')->onDelete('cascade');
				$table->integer('producto_id')->unsigned()->index();
				$table->foreign('producto_id')->references('id')->on('producto')->onDelete('cascade');
				$table->integer('cantidad');
				$table->timestamps();
			});
		}

		public function down() {
			Schema::dropIfExists('concepto_producto');
		}

	}
